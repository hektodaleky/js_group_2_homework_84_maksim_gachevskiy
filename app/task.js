const express = require('express');
const Task = require('../models/Task');
const usermiddleWare = require('../middleware/auth');

const ObjectID = require("mongodb").ObjectID;

const router = express.Router();

const createRouter = () => {
    router.get('/', usermiddleWare, (req, res) => {
        Task.find({user: req.user.id})
            .then(tasks => res.send(tasks))
            .catch(err => res.status(500).send("Request Error"))
    });

    router.post('/', usermiddleWare, (req, res) => {
        const task = new Task(req.body);
        task.user = req.user.id;
        task.save()
            .then(task => res.send(task))
            .catch(err => res.status(500).send(err))
    });

    router.put('/:id', usermiddleWare, (req, res) => {

        Task.findOne({_id: req.params.id})
            .then(task => {
                if (!ObjectID(task.user).equals(req.user.id))
                    res.status(404).send("Not Found");
                task.set(req.body);

                task.save()
                    .then(tsk => res.send(tsk))
                    .catch(err => res.status(500).send(err));

            })
            .catch(err => res.status(404).send("Not Found"))

    });

    router.delete('/:id', usermiddleWare, (req, res) => {
        Task.findOne({_id: req.params.id})
            .then(task => {
                if (!ObjectID(task.user).equals(req.user.id))
                    res.status(404).send("Not Found");
                else
                    Task.remove({_id: req.params.id})
                        .then(() => res.send(req.params.id))
                        .catch(err => res.status(500).send(err))
            })
            .catch(err => res.status(404).send("Not found"));

    });
    return router;
};

module.exports = createRouter;