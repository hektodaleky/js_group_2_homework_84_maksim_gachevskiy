const User = require('../models/User');

const auth = (req, res, next) => {
    const token = req.get('Token');

    if (!token) {
        return res.status(401).send({error: 'No token present'});

    }

    User.findOne({token: token})
        .then(user => {
            if (!user) {
                return res.status(401).send({error: 'User not found'});
            }

            req.user = user;
            next();
        })
        .catch(err => res.send(err));
};

module.exports = auth;