const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;

const TaskSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: String,
    status: {
        type: String,
        required: true,
        validate: {
            validator: value => {
                const correctValues = ["new", "in_progress", "complete"];
                console.log("Value", correctValues, value, correctValues.indexOf(value));
                return correctValues.indexOf(value) != -1
            },
            message: '{VALUE} is not correct, status must be ("new", "in_progress", "complete")'

        }
    }
});
const Task = mongoose.model('Task', TaskSchema);
module.exports = Task;